package com.example.fractalapp;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Path;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.SurfaceView;

import androidx.annotation.ColorInt;

import java.util.ArrayList;
import java.util.Random;
import java.util.Timer;
import java.util.TimerTask;

public class customSurfaceView extends SurfaceView {

    public static Paint mPaint;
    public static int[] bmpdata;
    public static int width=0;
    public static int height=0;
    public static Bitmap bmp;
    public static Path path;
    public static float[] points;
    public  static int pcount;
    int maxpcount;
    int[] colors;

    int PNUM;
    double[] Amp;
    double[] Freq;
    double[] Phase;

    double xcenter, ycenter;
    double dxy;

    int t;

    float px,py,ppx,ppy;
    int mcol;
    int r0,g0,b0;
    double phr0, phg0, phb0;

    public customSurfaceView(Context context, AttributeSet attrs) {
        super(context, attrs);

        this.setBackgroundColor(Color.BLACK);

        mPaint = new Paint();
        mPaint.setDither(true);
        mPaint.setColor(0xFFFFFF00);
        mPaint.setStyle(Paint.Style.STROKE);
        mPaint.setStrokeJoin(Paint.Join.ROUND);
        mPaint.setStrokeCap(Paint.Cap.ROUND);
        mPaint.setStrokeWidth(6);

        maxpcount = 100;
        pcount = 0;
        points = new float[maxpcount*2+4];
        colors= new int[maxpcount+2];

        //pointlist = new ArrayList < Float > ();


        Timer timer = new Timer();
        path = new Path();

        final int FPS = 40;
        TimerTask updateBall = new UpdateBallTask();
        timer.scheduleAtFixedRate(updateBall, 0, 1000 / FPS);

        path.moveTo(0,0);

        xcenter = 0.0;
        ycenter = 0.0;
        dxy = 0.01;

        t=0;

        PNUM=6;
        Amp = new double[PNUM];
        Freq= new double[PNUM];
        Phase= new double[PNUM];


    }


    @Override
    public boolean onTouchEvent(MotionEvent event) {

        //Log.d("AAAA","touch event. pcount:"+ String.valueOf(event.getPointerCount() ));

        //Log.d("AAAA","touch event hsize:"+String.valueOf(event.getHistorySize()));

        if(event.getAction()==MotionEvent.ACTION_DOWN)
        {
            Log.d("AAAA","touch event. action:"+ String.valueOf(event.getAction() ));
            t=0;

        }

        //Log.d("AAAA","touch event. pcount:"+ String.valueOf(event. ));

        return true;
    }

    public int getXPoint(double x)
    {

        int X= ((int)width/2) + (int)((x-xcenter)/dxy);

        //Log.d("AAAA", "getXpoint X:"+String.valueOf(x)+"width:"+String.valueOf(width) +"X:"+String.valueOf(X));
        return  X;

    }

    public int getYPoint(double y)
    {

        return ((int)height/2) + (int)((y-ycenter)/dxy);

    }

    public int createRandomBrightColor() {

        int R=new Random().nextInt(255);
        int G=new Random().nextInt(255);
        int B=new Random().nextInt(255);

        //Log.d("AAAA", "R=" + String.valueOf(R) +" G="+String.valueOf(G) + "B="+String.valueOf(B) );
        int max;
        if(R>=G && R>=B)
            max=R;
        else if(G>=R && G>=B)
            max=G;
        else
            max=B;

        if(max==0)
        {
            max=1;
        }



        R=(255*R)/max;
        G=(255*G)/max;
        B=(255*B)/max;

        //Log.d("AAAA", "R=" + String.valueOf(R) +" G="+String.valueOf(G) + "B="+String.valueOf(B) );


        return ((255<<24) | (R<<16) | (G<<8) | (B));

    }
    class UpdateBallTask extends TimerTask {

        int prevx=0;
        int prevy=0;
        int dx;
        int dy;
        int currentcolor;
        int t;

        public  UpdateBallTask(){
            t = 0;
        }


        public void run() {

            invalidate();
            /*
            if(width>0 || height>0)
            {
                t=t+1;

                points[t*2]=getXPoint(Math.sin(t*Math.PI/100.0));
                points[t*2+1]=getYPoint(Math.cos(t*Math.PI/100.0));
                colors[t]=createRandomBrightColor();
                colors[t]=((255<<24) | (255<<16) | (255<<8) | (255));

                invalidate();
                Log.d("AAAA","t=" + String.valueOf(t) + " draw points X"+String.valueOf(points[t*2]) + " Y:"+String.valueOf(points[t*2+1]) +"C:"+ colors[t]);

                pcount++;
            }
            */

            return;
            /*
            if(width>0 && height>0) {

                if(prevx==0 && prevy==0)
                {
                    dx = 0;
                    dy = 0;

                    while(Math.abs(dx)<20 || Math.abs(dy)<20) {
                        dx = new Random().nextInt(101) - 50;
                        dy = new Random().nextInt(101) - 50;
                    }

                    prevx = width/2;
                    prevy = height/2;
                    currentcolor = createRandomBrightColor();
                }


                int xnew = prevx + dx;
                int ynew = prevy+  dy;

                if(xnew<=0)
                {
                    dx=-dx;
                    ynew = ynew -xnew*dy/dx;
                    xnew = 0;

                    dx = dx + new Random().nextInt(8)-4;
                    dy = dy + new Random().nextInt(8)-4;
                    currentcolor = createRandomBrightColor();
                }
                if(xnew>=width)
                {
                    dx=-dx;
                    ynew = ynew -(xnew-width)*dy/dx;
                    xnew = width;

                    dx = dx + new Random().nextInt(8)-4;
                    dy = dy + new Random().nextInt(8)-4;
                    currentcolor = createRandomBrightColor();
                }

                if(ynew<=0)
                {
                    dy=-dy;
                    xnew = xnew -ynew*dx/dy;
                    ynew = 0;

                    dx = dx + new Random().nextInt(8)-4;
                    dy = dy + new Random().nextInt(8)-4;
                    currentcolor = createRandomBrightColor();
                }
                if(ynew>=height)
                {
                    dy=-dy;
                    xnew = xnew -(ynew-height)*dx/dy;
                    ynew = height;

                    dx = dx + new Random().nextInt(8)-4;
                    dy = dy + new Random().nextInt(8)-4;
                    currentcolor = createRandomBrightColor();
                }


                if(pcount==(maxpcount))
                {
                    for (int i = 0; i < (pcount-1); i++) {
                        points[i*2]=points[i*2+2];
                        points[i*2+1]=points[i*2+3];
                        colors[i]=colors[i+1];
                        //paints[pcount] = paints[pcount+1];
                    }


                    points[pcount*2-2] = xnew;
                    points[pcount*2-1] = ynew;

                    int r=new Random().nextInt(255);
                    int g=new Random().nextInt(255);
                    int b=new Random().nextInt(255);

                    colors[pcount-1]=currentcolor;

                }
                else
                {
                    points[pcount*2] = xnew;
                    points[pcount*2+1] = ynew;


                    colors[pcount]= currentcolor;


                    pcount++;
                }

                prevx = xnew;
                prevy = ynew;





                //path.reset();
                //path.moveTo(points[0],points[1]);

                //if(pcount>1) {
                //    for (int i = 1; i < pcount; i++) {
                //        path.lineTo(points[i*2],points[i*2+1]);
                //    }
                // }


            }
            */
        }
    };

    public void regenerateBitmapIfNecessary(Canvas canvas){
        if(width!=canvas.getWidth() || height!=canvas.getHeight())
        {
            width = canvas.getWidth();
            height = canvas.getHeight();
            bmpdata=new int[width*height* 4];

            int a,r,g,b;

            for(int y=0;y<height;y++)
            {
                for(int x=0;x<width;x++)
                {
                    a=255;
                    r=new Random().nextInt((x&0xFF)+1);
                    g=new Random().nextInt((y&0xFF)+1);
                    b=new Random().nextInt(((x+y)&0xFF)+1);

                    bmpdata[y*width+x]=(a<<24) | (r<<16) | (g<<8) | b;
                }
            }
            bmp = Bitmap.createBitmap(bmpdata,width,height, Bitmap.Config.ARGB_8888);
        }
    }

    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        width = canvas.getWidth();
        height = canvas.getHeight();

        double left =  - width*dxy/2;
        double right =  width*dxy/2;
        double top = height*dxy/2;
        double btm = -height*dxy/2;



        if(t==0)
        {
            //ppx=px;
            //ppy=py;

            px=0;
            py=0;

            double xmax=0;
            double ymax=0;

            for(int pi=0;pi<PNUM;pi++)
            {
                Amp[pi]=(new Random().nextFloat());
                Freq[pi]=(new Random().nextInt(10));
                Phase[pi]=(new Random().nextFloat());


                if (pi%2==0){
                    xmax = xmax + Math.abs(Amp[pi]);
                }
                else
                {
                    ymax = ymax + Math.abs(Amp[pi]);
                }

            }

            double aratiox = right/xmax;
            double aratioy = top/ymax;

            for(int pi=0;pi<PNUM;pi++)
            {
                if (pi%2==0){
                    Amp[pi]=Amp[pi]*aratiox;
                }
                else
                {
                    Amp[pi]=Amp[pi]*aratioy;
                }

            }
            mcol = createRandomBrightColor();

            r0 = (mcol >> 16) & 0xFF;
            g0 = (mcol >> 8) & 0xFF;
            b0 = mcol & 0xFF;

            phr0 = Math.asin((r0-127.5)/127.5);
            phg0 = Math.asin((g0-127.5)/127.5);
            phb0 = Math.asin((b0-127.5)/127.5);

            mPaint.setColor(mcol);

            path.reset();
        }

        px=0;
        py=0;
        for(int pi=0;pi<(PNUM/2);pi++){
            int pxi=pi*2;
            int pyi=pi*2+1;
            px = (float) (px+ Amp[pxi]*Math.cos(Freq[pxi]*t*Math.PI/400.0 + Phase[pxi]*2*Math.PI));
            py = (float) (py+ Amp[pyi]*Math.cos(Freq[pyi]*t*Math.PI/400.0 + Phase[pyi]*2*Math.PI));

        }
        px = getXPoint((double)px);
        py = getYPoint((double)py);


        int col,r,g,b;
        r= (int)(127.5+127.5*Math.sin(2*Math.PI*t/400 + phr0));
        g= (int)(127.5+127.5*Math.sin(2*Math.PI*t/400 + phg0));
        b= (int)(127.5+127.5*Math.sin(2*Math.PI*t/400 + phb0));

        //Log.d("AAAA","r="+String.valueOf(r)+"g="+String.valueOf(g)+"b="+String.valueOf(b));
        col = (255<<24) | (r<<16) | (g<<8) | b;



        mPaint.setColor(col);
        if(t==0)
        {
            path.moveTo(px,py);

        }
        else {
            path.lineTo(px, py);
            canvas.drawPath(path, mPaint);
        }

        //if(t==800)
        //{
        //   t=0;
        //}
        //else
        //{
        t=t+1;
        //}

/*
        for(int i=0;i<(pcount-1);i++)
        {

            mPaint.setColor(colors[i]);
            canvas.drawLine(points[i*2], points[i*2+1], points[i*2+2], points[i*2+3],mPaint);

            //path.reset();
            //path.moveTo(points[i*2],points[i*2+1]);
            //path.lineTo(points[i*2+2],points[i*2+3]);

           //
            //canvas.drawPath(path,mPaint);
        }
*/
        //canvas.drawPoints();
        /*
        if (_graphics1.size() > 0) {
            canvas.drawPath(_graphics1.get(_graphics1.size() - 1).getPath(),
                    _graphics1.get(_graphics1.size() - 1).getmPaint());
        }
        */
        //invalidate();
    }
}
